# Resin Assistant #

Resin Assistant is a platform for assiting resin printers. 
This project, code and models are licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

Resin printers have a few draw back to them. 

* Most have a lid that you have to lift up that tends to get dirty with resin and alcohol. 
* Lids when you pull them off the printer have to go on the floor or table which takes up space
* Most resins have a temperature that they need be within or they become runny and don't print well. 
* Most resin printers do not have the ability to modify them or add attachments
* Most resin printer firmware is propretary and does not allow you to mod it 

Resin assistant solves many of these problems.  

* Lifts the resin lid and replaces with a touch of a button
* Moves it safely above the printer not requiring additional floor or table space
* Optional heater and thermal control allows you to control your enclosures temperature
* Open Source Firmware you can modify for your needs
* Frame allows you to attach any number of tools to save space

### How to build? ###

Watch the YouTube video.

### Bill Of Materials (BOM) ###

[Resin Assistant BOM](https://docs.google.com/spreadsheets/d/1Qk7NxoAB9rGG1NuIru03CnwwLPtS5wztlZY7Z-UkVJc/edit?usp=sharing)

### How do I get started? ###

* Watch Video
* Download Code
* Configure Pins for your mainboard
* Build Resin Assistant
* Test your device
* Reward Yourself for a great build.

### Where do I get help? ###

Discord: https://discord.gg/ZDKq5N2 use the #resin-assistant channel. 

Can I email you a question for support?  
No. Use the discord channel. 

What about if it's a very specific question?
No. Use the discord channel. 

I have a suggestion what should do?
Use the Discord channel and tag @Rob in it. 

What if I found a bug?
Submit a bug report: https://bitbucket.org/makersmashup/resinassistant/issues?status=new&status=open

I want to make Resin Assistant better with a contribution. 
Great! Keep reading.

### Contribution guidelines ###

Is this something many people (not just a few) will find useful?
Do you know how to write code and make your own 3D Models?
Do you know how to submit a pull request?

If you answered yes to everything and still want to contribute please reach out to @rob or @dano on the discord about your contribution. 
We can discuss your contribution and how you can contribute to the project. 

### Who do I talk to? ###
Discord: https://discord.gg/ZDKq5N2 use the #resin-assistant channel. 
