/**
 * resin_assistant.ino
 * Resin Assistant - Copyright 2022 - Robert W. Mech (https://youtube.com/MakersMashup)
 * This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
 * 
 * DO NOT CONNECT YOUR HEATERS UNTIL BOARD PROGRAMMING IS COMPLETE
 * During programming the pin states can be unstable and not pull 
 * your AC relay to low.  Because of this we do not recommended powering 
 * MAIN AC to your Resin Assistant until you have completed flashing your
 * mainboard.  Failure to follow this can result in your heater running
 * without fans which could lead to device failure.
 */
#include <StaticThreadController.h>
#include <Thread.h>
#include <ThreadController.h>
#include <SpeedyStepper.h>
#include <Thermistor.h>
#include <NTC_Thermistor.h>
#include <AverageThermistor.h>
#include "melzi_pins.h"
#include "U8glib.h"
#include "EncoderStepCounter.h"
#include <Bounce2.h>

SpeedyStepper stepper;
Thread lightedButtons = Thread();
int lightIntensity=0;
bool lightDir = false;
bool moveDownFlag=false;

// ** CONFIGURATION **
#define DEBUGMODE         false               // Enabling this will cause everything to run slower, disable for production
#define INFOMODE          false                // Enabling this will give some basic info to the serial port

// Remap pins from header file to normal pins here, grab a pin file from marlin or create your own.  See melzi_pins.h as an example. 
// Stepper Pins (Required)
#define ENABLE_PIN        ORIG_X_ENABLE_PIN   // Stepper Enable
#define STEP_PIN          ORIG_X_STEP_PIN     // Stepper Step Pin
#define DIR_PIN           ORIG_X_DIR_PIN      // Stepper Direction

// Buttons and Led pins (required)
#define UP_BUTTON_LED     HEATER_0_PIN        // LED for up button (12v, use a heater or heatbed pin)
#define DOWN_BUTTON_LED   HEATER_1_PIN        // LED for down button (12v, use a heater or heatbed pin)
#define LIGHT_SPEED       75                   // Speed of lighting updates (Smaller is more frequent updates)
#define LIGHT_STEPS       25                    // Lighting effect steps (larger is a faster transition)
#define DOWN_BUTTON       ORIG_Y_MIN_PIN      // Y min limit switch on board
#define UP_BUTTON         ORIG_X_MIN_PIN      // X Min limit switch on board
#define MOVE_DISTANCE     8700l               // That is an L at the end to denote long.  
                                              // If you change this leave the L or arduino does maths wrong. 
// Display Control - If set to true will use 12864 Discount rep rap display on Melizi ports
#define USE_DISPLAY       true                // Use display for showing temp and other information.
#define MOSI_PIN          17                  // Pins for Wire.h  - Configure for your 
#define SCK_PIN           30                  // Mainboard these are set for a MELZI
#define CS_PIN            28                  // Creality board
#define LCD_REFRESH       250                 // Display Refresh Frequency in ms
#define ENC_BUTTON        16                 // encoder click on Creality Melzi screen
#define BEEPER            27                  // BEEPER
#define ENCODER_PIN1      10
#define ENCODER_INT1 digitalPinToInterrupt(ENCODER_PIN1)
#define ENCODER_PIN2      11
#define ENCODER_INT2 digitalPinToInterrupt(ENCODER_PIN2)

// Temp Display/Control Configuration (Optional equipment)
// Thermistor I used, plugs right into melzi https://amzn.to/3u6HSKC  
#define USE_THERMISTOR    true                // Use thermistor for temprature Display (Requires thermistor setup and datasheet)
#define THERMISTOR_PIN    7                  // Z-Endstop Digital pin connected to the DHT sensor 
#if USE_THERMISTOR
  #define SENSOR_PIN             7
  #define REFERENCE_RESISTANCE   8000
  #define NOMINAL_RESISTANCE     175000       // Adjust for the nominal resistance value and temp. 
  #define NOMINAL_TEMPERATURE    25
  #define B_VALUE                3950
  #define READINGS_NUMBER 5
  #define DELAY_TIME 10
  #define DISPLAY_FORMAT         "C"          // (C)elcius (K)elvin or (F)ahrenheit 
  #define THERMISTOR_REFRESH     1000          // Thermistor refresh interval in ms
#endif

// PTC HEATER CONFIGURATION
#define USE_HEATER               true         // Enables heater on heatbed pin
#if USE_THERMISTOR && USE_HEATER  
  #define HEATER_FAN_PIN           4            // Using the fan pin to trigger the mosfet
  #define HEATER_PIN               20           // Fan for heater (Z-Endstop for Melzi)
  #define HEAT_DISABLED_MSG        "Heating Disabled" // Make an empty string to remove the message from the bottom of the display. 
  #define TEMP_SWING               2            // Number of degrees above or below target temp so heater is not on always
  boolean heaterEnabled=false;
#endif 

// *** END CONFIGURATION **

#if USE_THERMISTOR
Thread thermistorThread = Thread();
Thermistor* thermistor = NULL;
float currentTemp=0.00l;
float setTemp=0;
float heaterTemp=0;
#endif 

#if USE_DISPLAY
EncoderStepCounter encoder(ENCODER_PIN1, ENCODER_PIN2);
Bounce encButton = Bounce();
long position=0;
U8GLIB_ST7920_128X64_1X u8g(SCK_PIN, MOSI_PIN, CS_PIN);
// Thread init
Thread rrDisplay = Thread();
#endif

int activeLED=UP_BUTTON_LED;

void setup() {
  delay(1000);
  pinMode(ORIG_Z_MIN_PIN,OUTPUT);
  digitalWrite(ORIG_Z_MIN_PIN,HIGH);
  Serial.begin(9600);
  Serial.println("Resin Assistant v1.0 - (C) 2022 Makers Mashup"); 
  
  //  Thread Setup
  lightedButtons.onRun(buttonLightingCallback);
  lightedButtons.setInterval(LIGHT_SPEED);
#if USE_DISPLAY 
  rrDisplay.onRun(displayCallback);
  rrDisplay.setInterval(LCD_REFRESH);
#endif
#if USE_THERMISTOR 
  thermistorThread.onRun(thermistorCallback);
  thermistorThread.setInterval(THERMISTOR_REFRESH);
#endif


  // Pin Setup
  pinMode(STEP_PIN,OUTPUT);
  pinMode(DIR_PIN,OUTPUT);
  pinMode(LED_PIN,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  pinMode(DOWN_BUTTON_LED,OUTPUT);  
  pinMode(DOWN_BUTTON_LED,OUTPUT);  
  pinMode(HEATER_PIN, OUTPUT);
  pinMode(HEATER_FAN_PIN, OUTPUT);
  digitalWrite(HEATER_PIN, LOW);
  digitalWrite(HEATER_FAN_PIN, HIGH);
  // Enable Steppers
  digitalWrite(ENABLE_PIN, LOW);
  stepper.connectToPins(STEP_PIN, DIR_PIN);
  stepper.setSpeedInStepsPerSecond(5000l*16l);
  stepper.setAccelerationInStepsPerSecondPerSecond(150*16);
  
  Serial.print("Movement Distance: ");
  Serial.println(MOVE_DISTANCE);

#if USE_DISPLAY
  displaySetup(); // Setup Display
#endif

#if USE_THERMISTOR
  thermistorSetup();
#endif
}

void loop() {

    // Up down movement, simple check to be sure we're in the right position before moving up or down
    if(digitalRead(UP_BUTTON)==LOW && moveDownFlag==false){
        if(moveUp()){
          moveDownFlag=true;
        }
    }
    if(digitalRead(DOWN_BUTTON)==LOW && moveDownFlag==true){
        moveDown();
        moveDownFlag=false;
    }
    // Lighting special effects
    if(lightedButtons.shouldRun())
    lightedButtons.run();

    // If a display is attached use it and update the display
#if USE_DISPLAY
    if(rrDisplay.shouldRun())
     rrDisplay.run();        
#endif  
#if USE_THERMISTOR
    if(thermistorThread.shouldRun())
     thermistorThread.run();  
#endif

}

void buttonLightingCallback(){
  // Lighting Effect
  if(lightDir==false){ // Move Up
    lightIntensity+=LIGHT_STEPS;
  }else{
    lightIntensity-=LIGHT_STEPS;
  }
  if(lightIntensity>=255){
    lightIntensity=255;
    lightDir=true;
  }
  if(lightIntensity<=0){
    lightIntensity=0;
    lightDir=false;
  }
#if DEBUGMODE 
  Serial.print("LED PIN: " );
  Serial.print(activeLED);
  Serial.print(" Intensity: " );
  Serial.println(lightIntensity);
#endif  
  analogWrite(activeLED,lightIntensity);
  
}
boolean moveUp(){
#if USE_HEATER
      if(heaterEnabled==true){
        Serial.println("HEATER SHOULD NOT BE RUNING WHILE PRINTING");
         digitalWrite(HEATER_PIN, LOW);
         warningChime();
         warningChime();
         delay(1000);
         warningChime();
         warningChime();    
         delay(1000);
         warningChime();
         warningChime();     
         delay(1000);
         return false; // Exit and turn off heating. 
      }
#endif
      long movement = (MOVE_DISTANCE)*16l;
      Serial.print("Moving Up: ");
      Serial.println( (MOVE_DISTANCE)*16l);
      analogWrite(activeLED,255); 
      stepper.moveRelativeInSteps(movement);
      analogWrite(activeLED,0); 
      activeLED=DOWN_BUTTON_LED;
      return true;
}
void moveDown(){
      long movement = (-1*MOVE_DISTANCE)*16l;
      Serial.print("Moving Down: ");
      Serial.println(movement);
      analogWrite(activeLED,255); 
      stepper.moveRelativeInSteps(movement);
      analogWrite(activeLED,0); 
      activeLED=UP_BUTTON_LED;
}
