/**
 * thermistor.ino
 * Resin Assistant - Copyright 2022 - Robert W. Mech (https://youtube.com/MakersMashup)
 * This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
 * 
 * DO NOT CONNECT YOUR HEATERS UNTIL BOARD PROGRAMMING IS COMPLETE
 * During programming the pin states can be unstable and not pull 
 * your AC relay to low.  Because of this we do not recommended powering 
 * MAIN AC to your Resin Assistant until you have completed flashing your
 * mainboard.  Failure to follow this can result in your heater running
 * without fans which could lead to device failure.
 */
void thermistorSetup(){
      thermistor = new AverageThermistor(
      new NTC_Thermistor(
      SENSOR_PIN,
      REFERENCE_RESISTANCE,
      NOMINAL_RESISTANCE,
      NOMINAL_TEMPERATURE,
      B_VALUE
    ),
    READINGS_NUMBER,
    DELAY_TIME
  );
}
void thermistorCallback(){
  // Reads temperature
  if(DISPLAY_FORMAT=="C")
     currentTemp = thermistor->readCelsius();
  if(DISPLAY_FORMAT=="K")
     currentTemp = thermistor->readKelvin();
  if(DISPLAY_FORMAT=="F")
     currentTemp = thermistor->readFahrenheit();
  #if INFOMODE
    Serial.print("Current Temp from Thermistor is ");
    Serial.print(currentTemp);
    Serial.print("' ");
    Serial.println(DISPLAY_FORMAT);
  #endif
  if(setTemp==0){
    setTemp=round(currentTemp);
  }
  }
