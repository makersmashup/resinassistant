/**
 * graphicDisplay.ino
 * Resin Assistant - Copyright 2022 - Robert W. Mech (https://youtube.com/MakersMashup)
 * This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
 * 
 * This file controls the graphics and heating routines.  Modify this
 * file with care as you can create states in which the heater is
 * running. Running the heater without the fan can cause damage to the  
 * device. 
 * 
 */
#if USE_DISPLAY
  #if USE_HEATER
  boolean heating=false;
  #endif
  void displaySetup(){
    if ( u8g.getMode() == U8G_MODE_R3G3B2 ) {
      u8g.setColorIndex(255);     // white
    }
    else if ( u8g.getMode() == U8G_MODE_GRAY2BIT ) {
      u8g.setColorIndex(3);         // max intensity
    }
    else if ( u8g.getMode() == U8G_MODE_BW ) {
      u8g.setColorIndex(1);         // pixel on
    }
    else if ( u8g.getMode() == U8G_MODE_HICOLOR ) {
      u8g.setHiColorByRGB(255,255,255);
    }
  encoder.begin();
  attachInterrupt(ENCODER_INT1, encoderInterrupt, CHANGE);
  attachInterrupt(ENCODER_INT2, encoderInterrupt, CHANGE);
  pinMode(ENC_BUTTON, INPUT_PULLUP);
  encButton.attach(ENC_BUTTON);
  encButton.interval(250); // interval in ms
  welcomeChime();
  }
  // Thread called to update display
  void displayCallback(){
    u8g.firstPage();  
    do {
      draw();
    } while( u8g.nextPage() );
    displayEncoderUpdate(); // Update Encoder
  }
  
  void draw(void) {
    encButton.update();
    #if USE_HEATER
    if(encButton.changed() && encButton.read()==LOW){
      heaterEnabled=!heaterEnabled;
      acknowledgeChime(heaterEnabled==true ? true : false);
    }
    #endif
    
    u8g.setFont(u8g_font_helvB08);
    u8g.drawStr( 20, 8, "Makers Mashup's");
    u8g.setFont(u8g_font_helvB12r);
    u8g.drawStr( 1, 22, "Resin Assistant");
    // Show temp should we have the thermistor enabled
    #if USE_THERMISTOR
      u8g.setFont(u8g_font_helvB24);
      String tmpStr=String(currentTemp)+" "+DISPLAY_FORMAT;
      char tmpString[30];
      tmpStr.toCharArray(tmpString,10);
      u8g.drawStr(10, 51,tmpString);

#if USE_HEATER
    if(heaterEnabled==false){
      u8g.setFont(u8g_font_helvB08);
      u8g.drawStr( 10, 64, "--> Heater Offline <--");
    }else{
      u8g.setFont(u8g_font_helvB08);
      if(heating==true){
        tmpStr="-=> Heat: "+String(heaterTemp)+" "+DISPLAY_FORMAT+" <=-";
      }else{
        tmpStr="--> Heat: "+String(heaterTemp)+" "+DISPLAY_FORMAT+" <--";
      }
      tmpStr.toCharArray(tmpString,30);
      u8g.drawStr( 12, 64, tmpString );
    }
    // Check if we should turn heater on
    if(heaterTemp<=(currentTemp-2)){
      heating=false;
    }
    if(heaterTemp>=(currentTemp+2)){
      heating=true;
    }
    // Set heater State
    if(heaterEnabled==true){
      digitalWrite(HEATER_PIN, heating == true ? HIGH : LOW);
      digitalWrite(HEATER_FAN_PIN, HIGH); // FAn always on while heaters are enabled for safety
    }else{
      digitalWrite(HEATER_PIN, LOW);
        digitalWrite(HEATER_FAN_PIN, LOW);
        digitalWrite(HEATER_FAN_PIN, LOW);
      }
#else
      u8g.setFont(u8g_font_helvB08);
      u8g.drawStr( 22, 64, HEAT_DISABLED_MSG );
#endif
 
    #endif    
  }
#endif

void encoderInterrupt() {
  encoder.tick();
  heaterTemp=setTemp+(position*.1l);
}
void displayEncoderUpdate(){
    signed char pos = encoder.getPosition();
  if (pos != 0) {
    position += pos;
    encoder.reset();
  }
 
}
